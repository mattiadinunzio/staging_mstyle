// HDR
// var params = {
//     envMap: "HDR",
//     projection: 'normal',
//     roughness: 1.0,
//     bumpScale: 0.3,
//     background: false,
//     exposure: 1.0,
//     side: 'front'
// };
// var hdrCubeMap;

// var hdrCubeRenderTarget;

// FINE HDR



var type = "";
var hideMenu = function (typeShow) {
    document.getElementById('mainMenu').style.display = "none";
    switch (typeShow) {

        case "executive":

            break;

        case "taskChair":

            break;

        case "waiting":

            break;

        case "accessories":

            break;


        default:
            break;
    }
}

var sceltaModello = function (modello) {
    document.getElementById('schermataScelta').style.display = "none";
    switch (modello) {
        case "ZoeA":
            type = "ZoeA"
            break;

        default:
            break;
    }
}

// var scene, renderer, composer;
// var camera, cameraControls;
// var cube;
// var layerGenerale;
// var layerSeduta;
// var layerBraccioli;


// var changeType = function () {

// }

// var GLTFLoader = new THREE.GLTFLoader();
// var FBXLoader = new THREE.FBXLoader();

// var fabric = new THREE.TextureLoader().load('assets/fabric.jpg');
// fabric.wrapT = THREE.RepeatWrapping;
// fabric.wrapS = THREE.RepeatWrapping;
// fabric.repeat.set(10,10);
// var leather = new THREE.TextureLoader().load('assets/leather.jpg');

// var prezzoZoe = "";
// var prezzoMaterialeZoe = "";
// var prezzoOptional1 = "";
// var prezzoOptional2 = "";

// renderer = new THREE.WebGLRenderer({
//     antialias: true
// });

// renderer.setClearColor(0xe3e3e3);
// renderer.setSize(window.innerWidth, window.innerHeight);
// renderer.toneMapping = THREE.LinearToneMapping;
// renderer.shadowMap.enabled = true;
// renderer.shadowMap.type = THREE.PCFSoftShadowMap;

// document.getElementById('container').appendChild(renderer.domElement);
// // HDR 


// var genCubeUrls = function( prefix, postfix ) {
//     return [
//         prefix + 'px' + postfix, prefix + 'nx' + postfix,
//         prefix + 'py' + postfix, prefix + 'ny' + postfix,
//         prefix + 'pz' + postfix, prefix + 'nz' + postfix
//     ];
// };

// var ldrUrls = genCubeUrls( 'assets/', '.png' );

//
// new THREE.RGBELoader().load( 'assets/Modelli3D/studio015small.hdr', function( texture, textureData ) {
//     //console.log( textureData );
//     //console.log( texture );
//     texture.encoding = THREE.RGBEEncoding;
//     texture.minFilter = THREE.NearestFilter;
//     texture.magFilter = THREE.NearestFilter;
//     material.envMapIntensity = 2;
//     material.needsUpdate = true;
    
// } );

// // FINE HDR
// scene = new THREE.Scene();

// camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 10000);

// layerGenerale = new THREE.Object3D();
// layerGenerale.position.set(0, -0.5, 0)
// scene.add(layerGenerale);

// layerSeduta = new THREE.Object3D();
// layerSeduta.position.set(0, -0.5, 0);
// layerGenerale.add(layerSeduta);

// layerBraccioli = new THREE.Object3D();
// layerBraccioli.position.set(0, -0, 0);
// layerGenerale.add(layerBraccioli);

// camera.position.set(0, 2, 3);
// scene.add(camera);

// cameraControls = new THREE.OrbitControls(camera);
// cameraControls.enablePan = false;
// cameraControls.enableZoom = true;

// function init() {
//     var spotlight = new THREE.PointLight(0xffffff,0.4,100 );
//     spotlight.position.set(-2, 1, 1);
//     spotlight.castShadow = true;
//     scene.add(spotlight);

//     animate();
// };

// var planeShadowGeometry = new THREE.PlaneGeometry(100,100,20,20);
// var planeShadowMaterial = new THREE.MeshLambertMaterial({color:0xfefefe});
// var planeShadow = new THREE.Mesh(planeShadowGeometry,planeShadowMaterial);
// planeShadow.position.set(0,-0.5,0);
// planeShadow.rotation.x = - 1.5708 ;
// planeShadow.receiveShadow = true;
// scene.add(planeShadow)

//  new THREE.TextureLoader().load('assets/fabric.jpg');
// Caricamento Immagini Texture


// //////// ZOE A ////////


// // ZOE A SEDUTA

// var  zoeASedutaMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Base_Color.png');
// var  zoeASedutaHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Height.png');
// var  zoeASedutaMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Metallic.png');
// var  zoeASedutaAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Mixed_AO.png'); 
// var  zoeASedutaNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Normal.png');
// var  zoeASedutaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Roughness.png');

// // ZOE A STRUTTURA

// var  zoeAStrutturaMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Base_Color.png');
// var  zoeAStrutturaHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Height.png');
// var  zoeAStrutturaMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Metallic.png');
// var  zoeAStrutturaAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Mixed_AO.png'); 
// var  zoeAStrutturaNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Normal.png');
// var  zoeAStrutturaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Roughness.png');

// // ZOE A RUOTE

// var  zoeARuoteMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Base_Color.png');
// var  zoeARuoteHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Height.png');
// var  zoeARuoteMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Metallic.png');
// var  zoeARuoteAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Mixed_AO.png'); 
// var  zoeARuoteNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Normal.png');
// var  zoeARuoteRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Roughness.png');

// // ZOE A METAL

// var  zoeAPiediMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Base_Color.png');
// var  zoeAPiediHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Height.png');
// var  zoeAPiediMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Metallic.png');
// var  zoeAPiediAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Mixed_AO.png'); 
// var  zoeAPiediNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Normal.png');
// var  zoeAPiediRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Roughness.png');
 
// //// ZOE B /////



// // creazione MATERIALI


// /////// ZOE A //////


// var zoeAMaterialSeduta = new THREE.MeshStandardMaterial({
//     map:zoeASedutaMap,
//     aoMap:zoeASedutaAOMap,
//     metalnessMap:zoeASedutaMetalMap,
//     normalMap: zoeASedutaNormalMap,
//     roughness:0.7,
 

// });
// var zoeAMaterialStruttura = new THREE.MeshStandardMaterial({
//     map:zoeAStrutturaMap,
//     aoMap:zoeAStrutturaAOMap,
//     metalnessMap:zoeAStrutturaMetalMap,
//     normalMap: zoeAStrutturaNormalMap,
//     roughness:0.5
// });
// var zoeAMaterialMetal = new THREE.MeshStandardMaterial({
//     map:zoeAPiediMap,
//     aoMap:zoeAPiediAOMap,
//     metalnessMap:zoeAPiediMetalMap,
//     normalMap: zoeAPiediNormalMap,
//     roughness:0.5
// });
// var zoeAMaterialRuote = new THREE.MeshStandardMaterial({
//     map:zoeARuoteMap,
//     aoMap:zoeARuoteAOMap,
//     metalnessMap:zoeARuoteMetalMap,
//     normalMap: zoeARuoteNormalMap,
//     roughness:0.5   
// });
// // fine Creazione materiali

// FINE HDR




// var insert = function () {
//     layerGenerale.remove(layerSeduta);
//     layerSeduta = new THREE.Object3D();
//     layerGenerale.add(layerSeduta);
//     console.log('WHY?');
//     FBXLoader.load('assets/Modelli3D/ZoeA.fbx', function (glb) {
//         glb.traverse(function (child) {
//             child.getObjectByName(type + "_Seduta").scale.set(0.01, 0.01, 0.01);
//             child.getObjectByName(type + "_Seduta").castShadow = true;
//             layerSeduta.add(child.getObjectByName(type + "_Seduta"));
//             child.getObjectByName(type + "_Struttura").scale.set(0.01, 0.01, 0.01);
//             child.getObjectByName(type + "_Struttura").castShadow = true;
//             layerSeduta.add(child.getObjectByName(type + "_Struttura"));
//             child.getObjectByName(type + "_Metal").scale.set(0.01, 0.01, 0.01);
//             child.getObjectByName(type + "_Metal").castShadow = true;
//             layerSeduta.add(child.getObjectByName(type + "_Metal"));
//             child.getObjectByName(type + "_Ruote").castShadow = true;
//             child.getObjectByName(type + "_Ruote").scale.set(0.01, 0.01, 0.01)
//             layerSeduta.add(child.getObjectByName(type + "_Ruote"));
//         });
//     });
//     document.getElementById('containerMaterial').style.display = "block";
//     document.getElementById('braccioliContainer').style.display = "block";
//     document.getElementById('containerOptional').style.display = "block";
//     document.getElementById('containerShopCart').style.display = "block";
//     baseModel();
//     layerSeduta.castShadow = true;
// };
// var insert2 = function () {

//     layerGenerale.remove(layerSeduta);

//     layerSeduta = new THREE.Object3D();

//     layerGenerale.add(layerSeduta);

//     console.log('WHY?');

//         FBXLoader.load('assets/Modelli3D/ZoeB.fbx', function (glb) {
//         glb.traverse(function (child) {
//             child.getObjectByName("ZoeB_Seduta").scale.set(0.01, 0.01, 0.01)
//             layerSeduta.add(child.getObjectByName("ZoeB_Seduta"));
//             child.getObjectByName("ZoeB_Struttura").scale.set(0.01, 0.01, 0.01)
//             layerSeduta.add(child.getObjectByName("ZoeB_Struttura"));
//             child.getObjectByName("ZoeB_Metal").scale.set(0.01, 0.01, 0.01)
//             layerSeduta.add(child.getObjectByName("ZoeB_Metal"));
//             child.getObjectByName("ZoeB_Ruote").scale.set(0.01, 0.01, 0.01)
//             layerSeduta.add(child.getObjectByName("ZoeB_Ruote"));
//         });
//     });
//     document.getElementById('containerMaterial').style.display = "block";
//     document.getElementById('braccioliContainer').style.display = "block";
//     document.getElementById('containerOptional').style.display = "block";
//     document.getElementById('containerShopCart').style.display = "block";

//     baseModel();

// };
// var noSedia = function () {
//     layerGenerale.remove(layerSeduta);
//     layerSeduta = new THREE.Object3D();
//     layerGenerale.add(layerSeduta);
//     document.getElementById('containerMaterial').style.display = "none";
//     layerGenerale.remove(layerBraccioli);
//     layerBraccioli = new THREE.Object3D();
//     layerGenerale.add(layerBraccioli);
//     document.getElementById('braccioliContainer').style.display = "none";
//     document.getElementById('containerOptional').style.display = "none";
//     document.getElementById('containerShopCart').style.display = "none";

//     containerModelPrice.innerHTML = "";
//     containerOptionalsPrice.innerHTML = "";
//     containerOptionalsPrice2.innerHTML = "";
//     containerTotalPrice.innerHTML = "";
//     noBraccioliCart();
// };


// var insertBracciolo = function () {
//     layerGenerale.remove(layerBraccioli);
//     layerBraccioli = new THREE.Object3D();
//     layerGenerale.add(layerBraccioli);
//     console.log('WHY?');
//     GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
//         glb.scene.traverse(function (child) {
//             layerBraccioli.add(child.getObjectByName("Braccioli_Rock"));
//         });
//     });
//     bracciolo1Cart();

// };


// var insertBracciolo2 = function () {
//     layerGenerale.remove(layerBraccioli);
//     layerBraccioli = new THREE.Object3D();
//     layerGenerale.add(layerBraccioli);
//     console.log('WHY?');
//     GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
//         glb.scene.traverse(function (child) {
//             layerBraccioli.add(child.getObjectByName("Braccioli_Tekno"));
//         });
//     });
//     bracciolo2Cart()


// };

// var insertBracciolo3 = function () {
//     layerGenerale.remove(layerBraccioli);
//     layerBraccioli = new THREE.Object3D();
//     layerGenerale.add(layerBraccioli);
//     console.log('WHY?');
//     GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
//         glb.scene.traverse(function (child) {
//             layerBraccioli.add(child.getObjectByName("Braccioli_ginger"));
//         });
//     });
//     bracciolo3Cart()


// };

// var noBraccioli = function () {
//     layerGenerale.remove(layerBraccioli);
//     layerBraccioli = new THREE.Object3D();
//     layerGenerale.add(layerBraccioli);
//     noBraccioliCart()

// }

// var leatherMaterial = function () {
//     layerSeduta.children[0].material = zoeAMaterialSeduta;
//     layerSeduta.children[1].material = zoeAMaterialStruttura;
//     layerSeduta.children[2].material = zoeAMaterialMetal;
//     layerSeduta.children[3].material = zoeAMaterialRuote;
//     // new THREE.MeshLambertMaterial({
//     //     map: leather
//     // })
// }
// var fabricMaterial = function () {
//     layerSeduta.children[0].material = new THREE.MeshLambertMaterial({
//         map: fabric
//     })
// }

// function animate() {
//     requestAnimationFrame(animate);
//     cameraControls.update()
//     render();
// }

// init();

// function render() {
//     var PIseconds = Date.now() * Math.PI;
//     renderer.render(scene, camera);
// }

// // CART 
// var modelPrice = 378;
// var optionalPrice = 7;
// var optional1Price = 14;
// var bracciolo1 = 12;
// var bracciolo2 = 22;
// var checkBox = document.getElementById('myCheck');
// var checkBox2 = document.getElementById('myCheck2');
// var containerModelPrice = document.getElementById('modelPrice');
// var containerBraccioloPrice = document.getElementById('braccioloPrice');
// var containerOptionalsPrice = document.getElementById('optionals');
// var containerOptionalsPrice2 = document.getElementById('optionals2');
// var containerTotalPrice = document.getElementById('totalPrice');

// var baseModel = function () {
//     modelPrice = 378;
//     optional1Price = 0;
//     optionalPrice = 0;
//     bracciolo = 0;
//     bracciolo2 = 0;
//     containerModelPrice.innerHTML = "";
//     containerOptionalsPrice.innerHTML = "";
//     containerOptionalsPrice2.innerHTML = "";
//     checkBox2.checked = false;
//     checkBox.checked = false;
//     containerModelPrice.innerHTML = "<p> Model Zoe : " + modelPrice + ",00 €</p>";
//     total();
// }
// var bracciolo1Cart = function () {
//     containerBraccioloPrice.innerHTML = "";
//     bracciolo = 12;

//     containerBraccioloPrice.innerHTML = '<p> Braccioli Rock : ' + bracciolo + ',00 €</p>'

//     total();
// }
// var bracciolo2Cart = function () {
//     containerBraccioloPrice.innerHTML = "";
//     bracciolo = 22;

//     containerBraccioloPrice.innerHTML = '<p> Braccioli Tekno : ' + bracciolo + ',00 €</p>'

//     total();
// }

// var bracciolo3Cart = function () {
//     containerBraccioloPrice.innerHTML = "";
//     bracciolo = 35;

//     containerBraccioloPrice.innerHTML = '<p> Braccioli Ginger : ' + bracciolo + ',00 €</p>'

//     total();
// }
// var noBraccioliCart = function () {
//     containerBraccioloPrice.innerHTML = "";
//     bracciolo = 0;

//     total();
// }


// var optional = function () {

//     var s = containerOptionalsPrice.innerHTML;

//     if (checkBox.checked == true) {

//         optionalPrice = 7;

//         s += '<p> Optional : ' + optionalPrice + ',00 €</p>';

//     } else

//     {

//         s = s.replace('<p> Optional : ' + optionalPrice + ',00 €</p>', "");

//         optionalPrice = 0;


//     }

//     containerOptionalsPrice.innerHTML = s;

//     total();


// }

// var optional1 = function () {

//     var s = containerOptionalsPrice2.innerHTML;

//     if (checkBox2.checked == true) {
//         optional1Price = 14;

//         s += '<p> Optional : ' + optional1Price + ',00 €</p>';

//     } else {
//         s = s.replace('<p> Optional : ' + optional1Price + ',00 €</p>', "");

//         optional1Price = 0;
//     }
//     containerOptionalsPrice2.innerHTML = s;

//     total();
// }

// var total = function () {
//     totalPrice = modelPrice + optionalPrice + optional1Price + bracciolo;

//     containerTotalPrice.innerHTML = "<p class='nameSezione'> Total : " + totalPrice + ",00 €</p>";
// }
// ERGOTEKITALIA