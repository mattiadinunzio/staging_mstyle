var scene, renderer, composer;
var camera, cameraControls;


var params = {
    envMap: "HDR",
    projection: 'normal',
    roughness: 1.0,
    bumpScale: 0.3,
    background: false,
    exposure: 1.0,
    side: 'front'
};
var hdrCubeMap;

var hdrCubeRenderTarget;

var GLTFLoader = new THREE.GLTFLoader();
var FBXLoader = new THREE.FBXLoader();

function init() {
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true 
    });

    renderer.setClearColor(0xeeeeee);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.toneMapping = THREE.LinearToneMapping;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    document.getElementById('container').appendChild(renderer.domElement);


    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 10000);


    camera.position.set(0, 0, 2.6);
    scene.add(camera);

    cameraControls = new THREE.OrbitControls(camera);
    cameraControls.enablePan = false;
    cameraControls.enableZoom = false;


    var spotlight = new THREE.PointLight(0xffffff,0.8,100 );
    spotlight.position.set(-2, 1, 1);
    scene.add(spotlight);
       
    
    var spotlight3 = new THREE.AmbientLight(0xeeeeee,0.2 ,100 );
    spotlight3.position.set(0, 0, -1);
    scene.add(spotlight3);
    loadTextureAndMaterials();

    createLayerHierarchy();

    animate();
};


function animate() {
    requestAnimationFrame(animate);
    cameraControls.update();
    TWEEN.update();
    render();
}

init();

function render() {
    var PIseconds = Date.now() * Math.PI;
    renderer.render(scene, camera);
}