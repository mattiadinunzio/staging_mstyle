var prezzoZoe = "";
var prezzoMaterialeZoe = "";
var prezzoOptional1 = "";
var prezzoOptional2 = "";

var modelPrice = 378;
var optionalPrice = 7;
var optional1Price = 14;
var optional2Price = 33;
var bracciolo1 = 12;
var bracciolo2 = 22;
var checkBox = document.getElementById('myCheck');
var checkBox2 = document.getElementById('myCheck2');
var checkBox3 = document.getElementById('myCheck3');

var containerModelPrice = document.getElementById('modelPrice');
var containerBraccioloPrice = document.getElementById('braccioloPrice');
var containerOptionalsPrice = document.getElementById('optionals');
var containerOptionalsPrice2 = document.getElementById('optionals2');
var containerOptionalsPrice3 = document.getElementById('optionals2');

var containerTotalPrice = document.getElementById('totalPrice');

var baseModel = function () {
    modelPrice = 378;
    optional1Price = 0;
    optionalPrice = 0;
    bracciolo = 0;
    bracciolo2 = 0;
    containerModelPrice.innerHTML = "";
    containerOptionalsPrice.innerHTML = "";
    containerOptionalsPrice2.innerHTML = "";
    checkBox3.checked = false;
    checkBox2.checked = false;
    checkBox.checked = false;
    containerModelPrice.innerHTML = "<p> Model Zoe : " + modelPrice + ",00 €</p>";
    total();
}
var bracciolo1Cart = function () {
    containerBraccioloPrice.innerHTML = "";
    bracciolo = 12;

    containerBraccioloPrice.innerHTML = '<p> Braccioli Rock : +' + bracciolo + ',00 €</p>'

    total();
}
var bracciolo2Cart = function () {
    containerBraccioloPrice.innerHTML = "";
    bracciolo = 22;

    containerBraccioloPrice.innerHTML = '<p> Braccioli Tekno : +' + bracciolo + ',00 €</p>'

    total();
}

var bracciolo3Cart = function () {
    containerBraccioloPrice.innerHTML = "";
    bracciolo = 35;

    containerBraccioloPrice.innerHTML = '<p> Braccioli Ginger : + ' + bracciolo + ',00 €</p>'

    total();
}
var noBraccioliCart = function () {
    containerBraccioloPrice.innerHTML = "";
    bracciolo = 0;

    total();
}


var optional = function () {
    
    var s = containerOptionalsPrice.innerHTML;

    if (checkBox.checked == true) {
        piediAlluminium();
        optionalPrice = 7;

        s += '<p> Base in alluminio : + ' + optionalPrice + ',00 €</p>';

    } else

    {
        piediClassic()
        s = s.replace('<p> Base in alluminio : + ' + optionalPrice + ',00 €</p>', "");

        optionalPrice = 0;


    }

    containerOptionalsPrice.innerHTML = s;
    
    total();


}

var optional1 = function () {

    var s = containerOptionalsPrice2.innerHTML;

    if (checkBox2.checked == true) {
        optional1Price = 14;

        s += '<p> Ruote Gommate : + ' + optional1Price + ',00 €</p>';

    } else {
        s = s.replace('<p> Ruote Gommate : + ' + optional1Price + ',00 €</p>', "");

        optional1Price = 0;
    }
    containerOptionalsPrice2.innerHTML = s;

    total();
}


var optional2 = function () {

    var s = containerOptionalsPrice3.innerHTML;

    if (checkBox3.checked == true) {
        optional2Price = 33;
     
        // tween.start();

        s += '<p> Syncro 4 pos. : + ' + optional2Price + ',00 €</p>';

    } else {
        s = s.replace('<p> Syncro 4 pos. : + ' + optional2Price + ',00 €</p>', "");
        
        // tween.stop();
        optional2Price = 0;
    }
    containerOptionalsPrice3.innerHTML = s;

    total();
}
var total = function () {
    totalPrice = modelPrice + optionalPrice + optional1Price + bracciolo;

    containerTotalPrice.innerHTML = "<p class='nameSezione'> Total : " + totalPrice + ",00 €</p>";
}