<?php  
 $message = '';  
 $error = '';  
 if(isset($_POST["submit"]))  
 {  
      if(empty($_POST["NomeModello"]))  
      {  
           $error = "<label class='text-danger'>Enter Name</label>";  
      }  
      else if(empty($_POST["colore"]))  
      {  
           $error = "<label class='text-danger'>Enter Gender</label>";  
      }  
      else  
      {  
           if(file_exists('data.json'))  
           {  
                $current_data = file_get_contents('data.json');  
               
                $array_data = json_decode($current_data, true);  

                $extra = array(  
                     'NomeModello'     =>     $_POST['NomeModello'],

                     'GenereModello'   =>     $_POST['GenereModello'],
                     
                     'CodiceModello'   =>     $_POST['CodiceModello'],  

                     'colore'          =>     $_POST["colore"],  
                     
                     'BraccioliDisponibili' =>  array(
                     
                        'braccioli'    =>     $_POST['selectBraccioli'],
                     
                     
                     ),
                    'materiali' =>  array(
                         'materiali'    =>     $_POST['selectMateriali']
                    )

                );  
                
                $array_data[] = $extra;  
                
                $final_data = json_encode($array_data);  
               
                if(file_put_contents('data.json', $final_data))  
                {  
                     $message = "<label class='text-success'>File Appended Success fully</p>";  
                }  
           }  
           else  
           {  
                $error = 'JSON File not exits';  
           }  
      }  
 }  
 ?>




<!DOCTYPE html>
<html>

<head>
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

</head>

<body>
    <br />
    <div class="container" style="width:500px;">
        <h3 align="">Append Data to JSON File</h3><br />
        <form method="post">
            <?php   
                     if(isset($error))  
                     {  
                          echo $error;  
                     }  
                     ?>
            <br />

            <!-- INPUT MODELLI SEDIE -->

            <label>Nome Modello</label>

            <input type="text" name="NomeModello" class="form-control" /><br />
            <br />
            <label>Genere Modello</label>

            <input type="text" name="GenereModello" class="form-control" /><br />
            <br />
            <label>Codice Modello</label>

            <input type="text" name="CodiceModello" class="form-control" /><br />
            <br />
            <label>Colore</label>

            <input type="text" name="braccioli1" class="form-control" /><br />
            <br />

            <br />
            <select name="selectBraccioli[]" size="3" multiple="multiple" tabindex="1">
                <option value="11">eleven</option>
                <option value="12">twelve</option>
                <option value="13">thirette</option>
                <option value="14">fourteen</option>
                <option value="15">fifteen</option>
            </select>
            <br />

            <br />
            <select name="selectMateriali[]" size="3" multiple="multiple" tabindex="1">
                <option value="11">eleven</option>
                <option value="12">twelve</option>
                <option value="13">thirette</option>
                <option value="14">fourteen</option>
                <option value="15">fifteen</option>
            </select>
            <br />            <br />

            <input type="submit" name="submit" value="Append" class="btn btn-info" /><br />



            <?php  
                     if(isset($message))  
                     {  
                          echo $message;  
                     }  
            ?>
        </form>

    </div>
    <br />
</body>

</html>