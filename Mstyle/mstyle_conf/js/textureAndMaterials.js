

// VARIABLES ZOE A

// CAGLI
var zoeASedutaMap;
var zoeASedutaMapCagliBrown;
var zoeASedutaMapCagliRed;
var zoeASedutaMapCagliBlack;
// BIBA

var zoeASedutaMapBibaGreen;
var zoeASedutaMapBibaOrange;
var zoeASedutaMapBibaGrey;

var  zoeASedutaHeightMap;
var  zoeASedutaMetalMap;
var  zoeASedutaAOMap;
var  zoeASedutaNormalMap;
var  zoeASedutaRoughnessMap;

var  zoeAStrutturaMap;
var  zoeAStrutturaHeightMap;
var  zoeAStrutturaMetalMap;
var  zoeAStrutturaAOMap;
var  zoeAStrutturaNormalMap;
var  zoeAStrutturaRoughnessMap;

var  zoeARuoteMap;
var  zoeARuoteHeightMap;
var  zoeARuoteMetalMap;
var  zoeARuoteAOMap;
var  zoeARuoteNormalMap;
var  zoeARuoteRoughnessMap;

var  zoeAPiediMap;
var  zoeAPiediHeightMap;
var  zoeAPiediMetalMap;
var  zoeAPiediAOMap;
var  zoeAPiediNormalMap;
var  zoeAPiediRoughnessMap;

var zoeAMaterialSeduta;
var zoeAMaterialSedutaBiba;
var zoeAMaterialStruttura;

var zoeAMaterialMetal;

var zoeAMaterialRuote;

// VARIABLES ZOE B
var  zoeBSedutaMap;
// CAGLI
var zoeBSedutaMapCagliBlack;
var zoeBSedutaMapCagliBrown;
var zoeBSedutaMapCagliBlue;
// BIBA

var zoeBSedutaMapBibaGreen;
var zoeBSedutaMapBibaOrange;
var zoeBSedutaMapBibaGrey;

var  zoeBSedutaHeightMap;
var  zoeBSedutaMetalMap;
var  zoeBSedutaAOMap;
var  zoeBSedutaNormalMap;
var  zoeBSedutaRoughnessMap;

var  zoeBStrutturaMap;
var  zoeBStrutturaHeightMap;
var  zoeBStrutturaMetalMap;
var  zoeBStrutturaAOMap;
var  zoeBStrutturaNormalMap;
var  zoeBStrutturaRoughnessMap;

var  zoeBRuoteMap;
var  zoeBRuoteHeightMap;
var  zoeBRuoteMetalMap;
var  zoeBRuoteAOMap;
var  zoeBRuoteNormalMap;
var  zoeBRuoteRoughnessMap;

var  zoeBPiediMap;
var  zoeBPiediHeightMap;
var  zoeBPiediMetalMap;
var  zoeBPiediAOMap;
var  zoeBPiediNormalMap;
var  zoeBPiediRoughnessMap;

var zoeBMaterialSeduta;

var zoeBMaterialStruttura;

var zoeBMaterialMetal;

var zoeBMaterialRuote;

var baseAllMap;
var baseAllHeightMap;
var baseAllMetalMap;
var baseAllNormalMap;
var baseAllRoughnessMap;

var baseAllMaterial;
var genCubeUrls;
var ldrUrls;
var zoeABibaNormalMap;
var zoeBBibaNormalMap;
var ldrCubeRenderTarget;

var loadTextureAndMaterials = function(){
        //////// ZOE A ////////


        // ZOE A SEDUTA

          /* BLUE */zoeASedutaMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Base_Color.png');

          // CAGLI
              zoeASedutaMapCagliBrown = new THREE.TextureLoader().load('assets/ZoeATex/Brown/ZoeA_seduta_Base_Color.png')
              zoeASedutaMapCagliRed = new THREE.TextureLoader().load('assets/ZoeATex/Red/ZoeA_seduta_Base_Color.png')
              zoeASedutaMapCagliBlack = new THREE.TextureLoader().load('assets/ZoeATex/Black/ZoeA_seduta_Base_Color.png')
          // BIBA
          zoeASedutaMapBibaGreen = new THREE.TextureLoader().load('assets/ZoeATex/biba/green/ZoeA_seduta_Base_Color.png')
          zoeASedutaMapBibaOrange = new THREE.TextureLoader().load('assets/ZoeATex/biba/orange/ZoeA_seduta_Base_Color.png')
          zoeASedutaMapBibaGrey = new THREE.TextureLoader().load('assets/ZoeATex/biba/grey/ZoeA_seduta_Base_Color.png')

          zoeABibaNormalMap = new THREE.TextureLoader().load('assets/ZoeATex/biba/green/ZoeA_seduta_Normal.png')


          zoeASedutaHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Height.png');
          zoeASedutaMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Metallic.png');
          zoeASedutaAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Mixed_AO.png'); 
          zoeASedutaNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Normal.png');
          zoeASedutaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_seduta_Roughness.png');

        // ZOE A STRUTTURA

          zoeAStrutturaMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Base_Color.png');
          zoeAStrutturaHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Height.png');
          zoeAStrutturaMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Metallic.png');
          zoeAStrutturaAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Mixed_AO.png'); 
          zoeAStrutturaNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Normal.png');
          zoeAStrutturaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_struttura_Roughness.png');

        // ZOE A RUOTE

          zoeARuoteMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Base_Color.png');
          zoeARuoteHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Height.png');
          zoeARuoteMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Metallic.png');
          zoeARuoteAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Mixed_AO.png'); 
          zoeARuoteNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Normal.png');
          zoeARuoteRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Ruote1_Roughness.png');

        // ZOE A METAL

          zoeAPiediMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Base_Color.png');
          zoeAPiediHeightMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Height.png');
          zoeAPiediMetalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Metallic.png');
          zoeAPiediAOMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Mixed_AO.png'); 
          zoeAPiediNormalMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Normal.png');
          zoeAPiediRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeATex/ZoeA_Metal1_Roughness.png');
        
        //// ZOE B /////


        // ZOE B SEDUTA

        // CAGLI //
        
        /*RED*/ zoeBSedutaMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Base_Color.png');

        zoeBSedutaMapCagliBlack = new THREE.TextureLoader().load('assets/ZoeBTex/cagli/Black/ZoeB_seduta_Base_Color.png');
        zoeBSedutaMapCagliBrown = new THREE.TextureLoader().load('assets/ZoeBTex/cagli/Brown/ZoeB_seduta_Base_Color.png');
        zoeBSedutaMapCagliBlue = new THREE.TextureLoader().load('assets/ZoeBTex/cagli/Blue/ZoeB_seduta_Base_Color.png');
        // BIBA 

        zoeBSedutaMapBibaGreen = new THREE.TextureLoader().load('assets/ZoeBTex/biba/Green/ZoeB_seduta_Base_Color.png');
        zoeBSedutaMapBibaOrange = new THREE.TextureLoader().load('assets/ZoeBTex/biba/Arancio/ZoeB_seduta_Base_Color.png');
        zoeBSedutaMapBibaGrey = new THREE.TextureLoader().load('assets/ZoeBTex/biba/Grigio/ZoeB_seduta_Base_Color.png');
        
        zoeBBibaNormalMap = new THREE.TextureLoader().load('assets/ZoeBTex/biba/green/ZoeB_seduta_Normal.png')
        
        zoeBSedutaHeightMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Height.png');
        zoeBSedutaMetalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Metallic.png');
        zoeBSedutaAOMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Mixed_AO.png'); 
        zoeBSedutaNormalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Normal.png');
        zoeBSedutaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_seduta_Roughness.png');

      // ZOE B STRUTTURA

        zoeBStrutturaMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Base_Color.png');
        zoeBStrutturaHeightMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Height.png');
        zoeBStrutturaMetalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Metallic.png');
        zoeBStrutturaAOMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Mixed_AO.png'); 
        zoeBStrutturaNormalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Normal.png');
        zoeBStrutturaRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_struttura_Roughness.png');

      // ZOE B RUOTE

        zoeBRuoteMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Base_Color.png');
        zoeBRuoteHeightMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Height.png');
        zoeBRuoteMetalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Metallic.png');
        zoeBRuoteAOMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Mixed_AO.png'); 
        zoeBRuoteNormalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Normal.png');
        zoeBRuoteRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_Ruote1_Roughness.png');

      // ZOE B METAL

        zoeBPiediMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Base_Color.png');
        zoeBPiediHeightMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Height.png');
        zoeBPiediMetalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Metallic.png');
        zoeBPiediAOMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Mixed_AO.png'); 
        zoeBPiediNormalMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Normal.png');
        zoeBPiediRoughnessMap =  new THREE.TextureLoader().load('assets/ZoeBTex/ZoeB_metal_Roughness.png');
      
      // BASE ALLUMINIO

        baseAllMap = new THREE.TextureLoader().load('assets/baseAllTex/piedistalloAll_Base_Color.png');
        baseAllHeightMap = new THREE.TextureLoader().load('assets/baseAllTex/piedistalloAll_Height.png');
        baseAllMetalMap = new THREE.TextureLoader().load('assets/baseAllTex/piedistalloAll_Metal.png');
        baseAllNormalMap = new THREE.TextureLoader().load('assets/baseAllTex/piedistalloAll_Normal.png');
        baseAllRoughnessMap = new THREE.TextureLoader().load('assets/baseAllTex/piedistalloAll_Roughness.png');

        // creazione MATERIALI


        /////// ZOE A //////

        // CAGLI //
         zoeAMaterialSeduta = new THREE.MeshStandardMaterial({
            map:zoeASedutaMap,
            aoMap:zoeASedutaAOMap,
            metalnessMap:zoeASedutaMetalMap,
            normalMap: zoeASedutaNormalMap,
            roughness:0.7
        });
        // BIBA // 
        zoeAMaterialSedutaBiba = new THREE.MeshStandardMaterial({
          
          aoMap:zoeASedutaAOMap,
          metalnessMap:zoeASedutaMetalMap,
          normalMap: zoeASedutaNormalMap,
          roughness:0.7
      });

         zoeAMaterialStruttura = new THREE.MeshStandardMaterial({
            map:zoeAStrutturaMap,
            aoMap:zoeAStrutturaAOMap,
            metalnessMap:zoeAStrutturaMetalMap,
            normalMap: zoeAStrutturaNormalMap,
            roughness:0.5
        });
         zoeAMaterialMetal = new THREE.MeshStandardMaterial({
            map:zoeAPiediMap,
            aoMap:zoeAPiediAOMap,
            metalnessMap:zoeAPiediMetalMap,
            normalMap: zoeAPiediNormalMap,
            roughness:0.5
        });
         zoeAMaterialRuote = new THREE.MeshStandardMaterial({
            map:zoeARuoteMap,
            aoMap:zoeARuoteAOMap,
            metalnessMap:zoeARuoteMetalMap,
            normalMap: zoeARuoteNormalMap,
            roughness:0.5   
        });

        /////// ZOE B MATERIAL //////

        // CAGLI //
        zoeBMaterialSeduta = new THREE.MeshStandardMaterial({
            map:zoeBSedutaMap,
            aoMap:zoeBSedutaAOMap,
            metalnessMap:zoeBSedutaMetalMap,
            normalMap: zoeBSedutaNormalMap,
            roughness:0.7
        });
        // BIBA // 
         zoeBMaterialStruttura = new THREE.MeshStandardMaterial({
            map:zoeBStrutturaMap,
            aoMap:zoeBStrutturaAOMap,
            metalnessMap:zoeBStrutturaMetalMap,
            normalMap: zoeBStrutturaNormalMap,
            roughness:0.5
        });
         zoeBMaterialMetal = new THREE.MeshStandardMaterial({
            map:zoeBPiediMap,
            aoMap:zoeBPiediAOMap,
            metalnessMap:zoeBPiediMetalMap,
            normalMap: zoeBPiediNormalMap,
            roughness:0.5
        });
         zoeBMaterialRuote = new THREE.MeshStandardMaterial({
            map:zoeBRuoteMap,
            aoMap:zoeBRuoteAOMap,
            metalnessMap:zoeBRuoteMetalMap,
            normalMap: zoeBRuoteNormalMap,
            roughness:0.5   
        });

        // BASE ALLUMINIO MATERIAL

        baseAllMaterial = new THREE.MeshStandardMaterial({
            color: 0xeeeeee,
            map: baseAllMap,
            metalnessMap: baseAllMetalMap,
            roughness: baseAllRoughnessMap

        })
        // fine Creazione materiali

         genCubeUrls = function( prefix, postfix ) {
            return [
                prefix + 'px' + postfix, prefix + 'nx' + postfix,
                prefix + 'py' + postfix, prefix + 'ny' + postfix,
                prefix + 'pz' + postfix, prefix + 'nz' + postfix
            ];
        };
        
         ldrUrls = genCubeUrls( 'assets/', '.png' );
        
        new THREE.CubeTextureLoader().load( ldrUrls, function ( ldrCubeMap ) {

            ldrCubeMap.encoding = THREE.GammaEncoding;

            var pmremGenerator = new THREE.PMREMGenerator( ldrCubeMap );
            pmremGenerator.update( renderer );

            var pmremCubeUVPacker = new THREE.PMREMCubeUVPacker( pmremGenerator.cubeLods );
            pmremCubeUVPacker.update( renderer );

            ldrCubeRenderTarget = pmremCubeUVPacker.CubeUVRenderTarget;

            material = new THREE.MeshStandardMaterial( {metalness:0.4,roughness:0.6,envMap: ldrCubeRenderTarget.texture, envMapIntensity:2 } );

            zoeAMaterialSeduta.envMap = ldrCubeRenderTarget.texture;
            zoeAMaterialSeduta.envMapIntensity =1.0;
            zoeAMaterialStruttura.envMap = ldrCubeRenderTarget.texture;
            zoeAMaterialStruttura.envMapIntensity =1.0;
            zoeAMaterialMetal.envMap = ldrCubeRenderTarget.texture;
            zoeAMaterialMetal.envMapIntensity =1.0;
            zoeAMaterialRuote.envMap = ldrCubeRenderTarget.texture;
            zoeAMaterialRuote.envMapIntensity =1.0;

            zoeBMaterialSeduta.envMap = ldrCubeRenderTarget.texture;
            zoeBMaterialSeduta.envMapIntensity =0.8;
            zoeBMaterialStruttura.envMap = ldrCubeRenderTarget.texture;
            zoeBMaterialStruttura.envMapIntensity =1.0;
            zoeBMaterialMetal.envMap = ldrCubeRenderTarget.texture;
            zoeBMaterialMetal.envMapIntensity =1.0;
            zoeBMaterialRuote.envMap = ldrCubeRenderTarget.texture;
            zoeBMaterialRuote.envMapIntensity =1.0;

            baseAllMaterial.envMap =  ldrCubeRenderTarget.texture;
            baseAllMaterial.envMapIntensity =1.0;

            
        } );

}