

var insertZoeA = function () {
    scene.remove(layerBraccioli);

    scene.remove(bigLayer);

    createLayerHierarchy();

    console.log('WHY?');

    bigLayer.name = "ZoeA";

    FBXLoader.load('assets/Modelli3D/ZoeA2.fbx', function (glb) {

        glb.traverse(function (child) {

            child.getObjectByName(type + "_Seduta").scale.set(0.01, 0.01, 0.01);
            layerSeduta.add(child.getObjectByName(type + "_Seduta"));

            child.getObjectByName(type + "_Struttura").scale.set(0.01, 0.01, 0.01);
            layerStruttura.add(child.getObjectByName(type + "_Struttura"));

            child.getObjectByName(type + "_Metal").scale.set(0.01, 0.01, 0.01);
            layerPiedi.add(child.getObjectByName(type + "_Metal"));

            child.getObjectByName('pistoncinoCromato').scale.set(0.01,0.01,0.01)
            layerPiedi.add(child.getObjectByName("pistoncinoCromato"))
            
            child.getObjectByName(type + "_Ruote").scale.set(0.01, 0.01, 0.01);
            layerRuote.add(child.getObjectByName(type + "_Ruote"));

        });
        bibaVerde();

    });
    document.getElementById('containerMaterial').style.display = "block";

    document.getElementById('braccioliContainer').style.display = "block";
    
    document.getElementById('containerShopCart').style.display = "block";
    
    baseModel();

    
    layerSeduta.castShadow = true;

};



var insertZoeB = function () {

    bigLayer.name = "ZoeB";

    scene.remove(layerBraccioli);

    scene.remove(bigLayer);

    createLayerHierarchy();

    console.log('WHY?');

        FBXLoader.load('assets/Modelli3D/ZoeB2.fbx', function (glb) {
        glb.traverse(function (child) {

            child.getObjectByName("ZoeB_Seduta").scale.set(0.01, 0.01, 0.01)
            layerSeduta.add(child.getObjectByName("ZoeB_Seduta"));

            child.getObjectByName("ZoeB_Struttura").scale.set(0.01, 0.01, 0.01)
            layerStruttura.add(child.getObjectByName("ZoeB_Struttura"));
          
            child.getObjectByName("ZoeB_Metal").scale.set(0.01, 0.01, 0.01)
            layerPiedi.add(child.getObjectByName("ZoeB_Metal"));
            
            child.getObjectByName('pistoncinoCromato').scale.set(0.01,0.01,0.01)
            layerPiedi.add(child.getObjectByName("pistoncinoCromato"))

            child.getObjectByName("ZoeB_Ruote").scale.set(0.01, 0.01, 0.01)
            layerRuote.add(child.getObjectByName("ZoeB_Ruote"));
        });
        cagliBlu();

    });
    document.getElementById('containerMaterial').style.display = "block";
    document.getElementById('braccioliContainer').style.display = "block";
    document.getElementById('containerShopCart').style.display = "block";

    baseModel();

};

var noSedia = function () {
    bigLayer.id = "ZoeB";
    scene.remove(layerBraccioli);

    scene.remove(bigLayer);
    
    createLayerHierarchy();

    document.getElementById('containerMaterial').style.display = "none";
  
    document.getElementById('braccioliContainer').style.display = "none";
    document.getElementById('containerShopCart').style.display = "none";

    containerModelPrice.innerHTML = "";
    containerOptionalsPrice.innerHTML = "";
    containerOptionalsPrice2.innerHTML = "";
    containerTotalPrice.innerHTML = "";
    noBraccioliCart();
};


var insertBracciolo = function () {

    layerObj.remove(layerBraccioli);

    createLayerBraccioli();


    console.log('WHY?');
    GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
        glb.scene.traverse(function (child) {
            layerBraccioli.add(child.getObjectByName("Braccioli_Rock"));
        });
    });

    bracciolo1Cart();

};


var insertBracciolo2 = function () {
    
    layerObj.remove(layerBraccioli);

    createLayerBraccioli();

    console.log('WHY?');
    GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
        glb.scene.traverse(function (child) {
            layerBraccioli.add(child.getObjectByName("Braccioli_Tekno"));
        });
    });
    bracciolo2Cart()


};

var insertBracciolo3 = function () {
    
    layerObj.remove(layerBraccioli);

    createLayerBraccioli();

    console.log('WHY?');
    GLTFLoader.load('assets/Modelli3D/allmodels.glb', function (glb) {
        glb.scene.traverse(function (child) {
            layerBraccioli.add(child.getObjectByName("Braccioli_ginger"));
        });
    });
    bracciolo3Cart()


};

var noBraccioli = function () {
    
    layerObj.remove(layerBraccioli);

    createLayerBraccioli();

    noBraccioliCart()

}


var piediClassic = function(){

    layerPiedi.children[0].material = zoeAMaterialMetal;

}

var piediAlluminium = function(){
    
    // FBXLoader.load('assets/Modelli3D/piedistalloAll.fbx', function (glb) {
    //     layerPiedi.remove(children[0]);
    //     glb.traverse(function(){
    //         child.getObjectByName('PiedistalloAll').scale.set(0.01,0.01,0.01);
    //         layerPiedi.add(child.getObjectByName('PiedistalloAll'));
    //     })
    // });
    layerPiedi.children[0].material = baseAllMaterial;
}